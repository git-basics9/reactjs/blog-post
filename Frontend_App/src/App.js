import './App.css';
import {Routes,Route} from 'react-router-dom'
import Navigationbar from './components/Navigationbar'
import Home from './components/Home'
import ShowBlog from './components/ShowBlog'
import EditBlog from './components/EditBlog'
import Nomatch from './components/Nomatch';
import About from './components/About'
import Contact from './components/Contact'
import './App.css'
function App() {
  return (
    <div className="App">
      <Navigationbar/>
      <Routes>
        <Route path='home' element={<Home/>}></Route>
        <Route path='/' element={<Home/>}></Route>
        <Route path='blog' element={<Home/>}></Route>

        <Route path='blog/:id' element={<ShowBlog/>}></Route>
        <Route path='blog/edit/:id' element={<EditBlog/>}></Route>

        <Route path='about' element={<About/>}></Route>
        <Route path='contact' element={<Contact/>}></Route>
        <Route path='*' element={<Nomatch/>}></Route>
      </Routes>
    </div>
  );
}

export default App;
