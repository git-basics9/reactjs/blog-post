import React, { useEffect, useState } from 'react'
import {useNavigate, useParams } from 'react-router-dom'
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';


function ShowBlog() {
  const [Blogs, setBlogs] = useState({});
  const {id} = useParams()
  const navigate = useNavigate()
  useEffect(() => {
     fetch(`http://localhost:3000/Blogs/${id}`).then(res=>res.json()).then(
      res=>{
        console.log(res);
        setBlogs(res)
    })
  },);
  if(Object.keys(Blogs).length === 0){
    return(<h3>Loading...</h3>)
  }
  else{
    return (
      <Card>
      <Card.Header as="h5">Blogs</Card.Header>
        <Card.Body key={Blogs.id}>
          <Card.Title style={{fontWeight:'700'}}>{Blogs.title}</Card.Title>
            <div style={{margin:'20px'}}>
          <Card.Text style={{textAlign:'justify'}}>
            {Blogs.description}
          </Card.Text>
            </div>
            <div style={{fontWeight:'700',margin:'30px'}}>
          <Card.Text>
            Author-:  {Blogs.authorName}
          </Card.Text>
            </div>
            <Button style={{margin:'5px'}} onClick={()=>navigate(`/blog/edit/${Blogs.id}`)} variant="primary">Edit</Button>
        </Card.Body>
      </Card>
    )
  }
}

export default ShowBlog