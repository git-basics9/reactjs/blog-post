import React from 'react'
import { NavLink } from 'react-router-dom'
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';


function Navigationbar() {
  // console.log(Blogs);
    const navigateStyle =  ({isActive})=>{
        return{
            fontWeight: isActive?'bold' :'normal',
            textDecoration: 'none',
            color:'white',
            fontSize: '1em'
        }
    }
  return (
    <>
      <Navbar bg="dark" variant="dark">
        <Container>
          <NavLink  style={navigateStyle} to="/">Navbar</NavLink>
          <Nav className="me-auto">
              <NavLink  style={navigateStyle} to="/">Home</NavLink>
              <NavLink  style={navigateStyle}to="about">About</NavLink>           
              <NavLink style={navigateStyle} to="contact">Contact</NavLink>
          </Nav>
        </Container>
      </Navbar>
    </>
  )
}

export default Navigationbar;