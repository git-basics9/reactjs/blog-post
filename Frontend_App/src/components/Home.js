import React, { useEffect, useState } from 'react'
import { useNavigate} from 'react-router-dom'
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';


function Home() {
  const navigate = useNavigate()
  const [Blogs, setBlogs] = useState([]);
  const [status, setStatus] = useState(true);

  useEffect(() => {
    fetch("http://localhost:3000/Blogs").then(res=>res.json()).then(res=>setBlogs(res))
  }, [status]);

  const deleteHandler = (e)=>{
    fetch(`http://localhost:3000/blogs/${e.target.id}`,{
      method:'DELETE'
    }).then(res=>res.json()).then(res=>{
      setStatus(!status)
    })
  }
  return (
    <Card>
      <Card.Header as="h5">{Blogs.length===0?"Loading...":"Blogs"}</Card.Header>
    {Blogs.map(ele=>(
          <>
        <Card.Body key={ele.id} id={ele.id}>
          <Card.Title>{ele.title}</Card.Title>
          <Card.Text>
            Author-: {ele.authorName}
          </Card.Text>
          <Button style={{margin:'5px'}} onClick={()=>navigate(`/blog/${ele.id}`)} variant="primary">Show Blog</Button>
          <Button style={{margin:'5px'}} onClick={()=>navigate(`/blog/edit/${ele.id}`)} variant="primary">Edit Blog</Button>
          <Button style={{margin:'5px'}} id={ele.id} onClick={deleteHandler} variant="primary">Delete Blog</Button>
        </Card.Body>
        </>
      ))}
      </Card>
  )
}

export default Home