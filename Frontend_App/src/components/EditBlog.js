import React, { useEffect, useState } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';

function EditBlog() {
  const [Blog, setBlog] = useState({});
  const [Title, setTitle] = useState('');
  const [Desc, setDesc] = useState('');
  const [Author, setAuthor] = useState('');
  const {id} = useParams()
  const navigate = useNavigate();
  useEffect(() => {
    fetch(`http://localhost:3000/Blogs/${id}`).then(res=>res.json()).then(
      res=>{
        console.log(res,id);
        setBlog(res)
        setTitle(res.title)
        setDesc(res.description)
        setAuthor(res.authorName)
      })
 },);
 
 const submitHandler = (e)=>{
   e.preventDefault()
   const newData = {
      title:Title,
      description:Desc,
      authorName:Author
    }
    fetch(`http://localhost:3000/Blogs/${id}`,{
      method: 'PUT',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(newData)
      })
      .then(res=>res.json())
      .then(res=>{
      console.log(res)
      navigate(-1)
      })
      .catch(res=>console.log("error",res))
  }

 if(Blog==={}){
  return (<h1>Loading</h1>)
 }
 else{
  return (
    <div className='form-container'>
    <Form onSubmit={submitHandler}>
      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>Edit Title</Form.Label>
        <Form.Control value={Title} onChange={(e)=>setTitle(e.target.value)} type="text"/>
      </Form.Group>
      <Form.Group className="mb-3" controlId="formBasicPassword">
        <Form.Label>Edit Description</Form.Label>
        <Form.Control value={Desc} style={{height:'100px'}} as="textarea" onChange={(e)=>setDesc(e.target.value)} type="text"/>
      </Form.Group>
      <Form.Group className="mb-3" controlId="formBasicPassword">
        <Form.Label>Edit Author Name</Form.Label>
        <Form.Control value={Author}  onChange={(e)=>setAuthor(e.target.value)} type="text" />
      </Form.Group>
      <Button style={{margin:'10px'}} variant="primary" type="submit">
        Save
      </Button>
      <Button variant="primary" onClick={()=>navigate(-1)}>
        Go Back
      </Button>
    </Form>
    </div>
  );
  }
}

export default EditBlog
